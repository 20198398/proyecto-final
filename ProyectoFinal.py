import numpy_financial as npf 
import tabulate as tab 
from datetime import datetime 
from dateutil.relativedelta import relativedelta


class amortizar:


    def solicitar(self):
        print(":::::::::::::::::::::::::::::CALCULADORA DE PRÉSTAMOS::::::::::::::::::::::::::::")
        print(" ")
        self.monto=float(input("Ingresar monto a evaluar: "))
        self.interes=float(input("Ingresar tasa de interés: "))
        self.cuotas=int(input("Ingresar cantidad cuotas (En meses): "))
        self.valcuota=float(input("Ingresar valor por cuota: "))
        self.Datos_De_Tabla = []
        self.balance = self.monto
        self.interes = self.interes * 0.001
        self.fecha_actual = datetime.now()
        

    def mostrar(self):
        print("========================================")
        print("DATOS INGRESADOS:")
        print("________________________________________")
        print("Monto del préstamo en RD$: ",self.monto)
        print("________________________________________")
        print("Tasa del porcentaje Anual: ",self.interes,"%")
        print("________________________________________")
        print("Plazo: ",self.cuotas," Meses")
        print("________________________________________")
        print("Valor Cuota: $",self.valcuota)
        print("========================================")

    def calcular(self):
        print ("++++++++++++++++++++++++++++++TABLA DE AMORTIZACION++++++++++++++++++++++++++++++")
        for i in range(1, self.cuotas + 1):
            self.capital_monto = npf.ppmt(self.interes,i,self.cuotas,-self.monto,0,0)
            self.total_inter = self.valcuota - self.capital_monto
            self.balance -= self.capital_monto
            self.fila = [i,format(self.fecha_actual+relativedelta(months=i),'%d %B %Y'), format(self.valcuota, '0,.0f'), format(self.capital_monto, '0,.0f'), format(self.total_inter, '0,.0f'), format(self.balance, '0,.0f')]
            self.Datos_De_Tabla.append(self.fila)
            self.endeuda = round((self.valcuota - self.total_inter) * 0.35)

        print(tab.tabulate(self.Datos_De_Tabla, headers=['Periodo','Fecha','Cuota','Capital','Intereses','Balance'], tablefmt='psql'))
        print("________________________________________________________________________________")
        print("+++Capacidad de endeudamiento: $",self.endeuda," de los ingresos mensuales.")
        print("________________________________________________________________________________")
        



resultado=amortizar()
resultado.solicitar()
resultado.mostrar()
resultado.calcular()

