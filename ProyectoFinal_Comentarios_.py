'''Desarrollé el código lo más entendible posible,pero para hacerlo más entendible, 
explicaré este proyecto a través de estos comentarios'''

import numpy_financial as npf #Aquí importo un módulo para poder calcular el total de intereses en la tabla. Se utiliza en Excel.
import tabulate as tab #Este módulo lo usaré para darle forma a la tabla, colocando los encabezados que quiero que aparezcan y el orden.
from datetime import datetime #Este módulo lo usaré para insertar la fecha en la tabla y poder calcularla.
from dateutil.relativedelta import relativedelta #Con este módulo calcularé los meses de las fechas de manera individual, para que--
#--aparezcan organizados en la tabla.


class amortizar:#La clase que contiene las funciones para realizar el cálculo y la tabla.


    def solicitar(self):#Esta función contiene los input para ingresar la información a procesar.
        print(":::::::::::::::::::::::::::::CALCULADORA DE PRÉSTAMOS::::::::::::::::::::::::::::")
        print(" ")
        self.monto=float(input("Ingresar monto a evaluar: "))
        self.interes=float(input("Ingresar tasa de interés: "))
        self.cuotas=int(input("Ingresar cantidad cuotas (En meses): "))
        self.valcuota=float(input("Ingresar valor por cuota: "))
        self.Datos_De_Tabla = []# Tabla en la que se hará la amortizacion
        self.balance = self.monto #Son valores dependientes, por lo que los relaciono.
        self.interes = self.interes * 0.001 #Operación para el cálculo interés (según pude investigar)
        self.fecha_actual = datetime.now()#variable con la que trabajaré para construir el campo de fechas en la tabla, junto al módulo necesario.

    def mostrar(self):#Función que recogerá los datos ingresados y los mostrará
        print("========================================")
        print("DATOS INGRESADOS:")
        print("________________________________________")
        print("Monto del préstamo en RD$: ",self.monto)
        print("________________________________________")
        print("Tasa del porcentaje Anual: ",self.interes,"%")
        print("________________________________________")
        print("Plazo: ",self.cuotas," Meses")
        print("________________________________________")
        print("Valor Cuota: $",self.valcuota)
        print("========================================")

    def calcular(self):#Función que hará la construcción de la tabla junto al cálculo completo.
        print ("++++++++++++++++++++++++++++++TABLA DE AMORTIZACION++++++++++++++++++++++++++++++")
        for i in range(1, self.cuotas + 1):#Uso for para poder crear un valor en cada fila de la tabla.
            self.capital_monto = npf.ppmt(self.interes,i,self.cuotas,-self.monto,0,0)#Cálculo de interes simple
            self.total_inter = self.valcuota - self.capital_monto#Cálculo de interes final para ingresarlo a su campo correspondiente
            self.balance -= self.capital_monto#El balance es calculado a medida se va restando por las filas.
            #La fila que sigue contiene el orden de aparición de la información calculada en la tabla, uso format con sus variables para organizarla mejor.
            self.fila = [i,format(self.fecha_actual+relativedelta(months=i),'%d %B %Y'), format(self.valcuota, '0,.0f'), format(self.capital_monto, '0,.0f'), format(self.total_inter, '0,.0f'), format(self.balance, '0,.0f')]
            #La parte fecha_actual es sumada con cada mes que pasa gracias al for establecido en el comienzo de la funcion.
            self.Datos_De_Tabla.append(self.fila)#Aquí se recogen las filas para la tabla
            self.endeuda = round((self.valcuota - self.total_inter) * 0.35)#Como no pude ubicar la capacidad de endeudamiento más arriba, la calculo aquí.

        print(tab.tabulate(self.Datos_De_Tabla, headers=['Periodo','Fecha','Cuota','Capital','Intereses','Balance'], tablefmt='psql'))
        #La sentencia anterior contiene el módulo tabular, que organiza la tabla junto a los encabezados que quiero poner, gracias al formato 'psql' de tablas.
        print("________________________________________________________________________________")
        print("+++Capacidad de endeudamiento: $",self.endeuda," de los ingresos mensuales.")#Aquí muestro la capacidad de endeudamiento.
        print("________________________________________________________________________________")
        


#Aquí se produce la ejecución del programa según el orden correcto de las funciones
resultado=amortizar()
resultado.solicitar()
resultado.mostrar()
resultado.calcular()
#Espero que el diseño de la tabla y el contenido sea de su agrado. ¡Gracias por su atención!
